#!/usr/bin/env python3

articulos = {}

def anadir(articulo, precio):
    articulos[articulo] = precio

def mostrar():
    print("Lista de artículos en la tienda:")
    for articulo, precio in articulos.items():
        print(f"{articulo}: {precio} euros")

def pedir_articulo() -> str:
    while True:
        articulo = input("Artículo: ")
        if articulo in articulos:
            return articulo
        else:
            print("El artículo no está en la lista de artículos. Inténtalo de nuevo.")

def pedir_cantidad() -> float:
    while True:
        cantidad = input("Cantidad: ")
        try:
            cantidad = float(cantidad)
            return cantidad
        except ValueError:
            print("La cantidad ingresada no es válida. Inténtalo de nuevo.")

def main():
    import sys

    args = sys.argv[1:]

    if len(args) % 2 != 0:
        print("Error en argumentos: Debes proporcionar un precio para cada artículo.")
        return

    for i in range(0, len(args), 2):
        articulo = args[i]
        precio = args[i + 1]
        if not precio.replace('.', '', 1).isdigit():
            print(f"Error en argumentos: {precio} no es un precio válido para {articulo}.")
            return
        anadir(articulo, float(precio))

    if not articulos:
        print("Error en argumentos: No se han especificado artículos.")
        return

    mostrar()
    articulo_compra = pedir_articulo()
    cantidad_compra = pedir_cantidad()

    total = articulos[articulo_compra] * cantidad_compra
    print(f"\nCompra total: {cantidad_compra} de {articulo_compra}, a pagar {total:.2f} euros")

if __name__ == '__main__':
    main()
